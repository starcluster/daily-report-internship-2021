\begin{thebibliography}{10}

\bibitem{PRL45}
K.~v. Klitzing, G.~Dorda, and M.~Pepper.
\newblock New method for high-accuracy determination of the fine-structure
  constant based on quantized hall resistance.
\newblock {\em Phys. Rev. Lett.}, 45:494--497, Aug 1980.

\bibitem{Landau1977}
L.~D. Landau and E.~M. Lifshitz.
\newblock {\em {Quantum Mechanics}}.
\newblock Pergamon, Oxford, England, UK, 1977.

\bibitem{PRL61}
F.~D.~M. Haldane.
\newblock Model for a quantum hall effect without landau levels:
  Condensed-matter realization of the "parity anomaly".
\newblock {\em Phys. Rev. Lett.}, 61:2015--2018, Oct 1988.

\bibitem{QHE_photonic}
S.~Raghu and F.~D.~M. Haldane.
\newblock {Analogs of quantum Hall effect edge states in photonic crystals}.
\newblock {\em arXiv}, Feb 2006.

\bibitem{Wang2008}
Zheng Wang, Y.~D. Chong, John~D. Joannopoulos, and Marin
  Solja{\ifmmode\check{c}\else\v{c}\fi}i{\ifmmode\acute{c}\else\'{c}\fi}.
\newblock {Reflection-Free One-Way Edge Modes in a Gyromagnetic Photonic
  Crystal}.
\newblock {\em Phys. Rev. Lett.}, 100(1):013905, Jan 2008.

\bibitem{topo_wiki}
{Contributeurs aux projets Wikimedia}.
\newblock {Topologie {\ifmmode---\else\textemdash\fi}
  Wikip{\ifmmode\acute{e}\else\'{e}\fi}dia}, Feb 2021.
\newblock [Online; accessed 19. Apr. 2021].

\bibitem{topo_faure}
{Géométrie et topologie pour la physique en Master 2 physique Université
  Grenoble-Alpes.}, Apr 2021.
\newblock [Online; accessed 29. Apr. 2021].

\bibitem{topo_photo_rev}
Ling Lu, John~D. Joannopoulos, and Marin
  Solja{\ifmmode\check{c}\else\v{c}\fi}i{\ifmmode\acute{c}\else\'{c}\fi}.
\newblock {Topological photonics}.
\newblock {\em Nat. Photonics}, 8:821--829, Nov 2014.

\bibitem{laser_topo}
Miguel~A. Bandres, Steffen Wittek, Gal Harari, Midya Parto, Jinhan Ren,
  Mordechai Segev, Demetrios~N. Christodoulides, and Mercedeh Khajavikhan.
\newblock {Topological insulator laser: Experiments}.
\newblock {\em Science}, 359(6381):eaar4005, Mar 2018.

\bibitem{photo_chip}
Han Zhao, Xingdu Qiao, Tianwei Wu, Bikashkali Midya, Stefano Longhi, and Liang
  Feng.
\newblock {Non-Hermitian topological light steering}.
\newblock {\em Science}, 365(6458):1163--1166, Sep 2019.

\bibitem{PRL119}
J.~Perczel, J.~Borregaard, D.~E. Chang, H.~Pichler, S.~F. Yelin, P.~Zoller, and
  M.~D. Lukin.
\newblock {Topological Quantum Optics in Two-Dimensional Atomic Arrays}.
\newblock {\em Phys. Rev. Lett.}, 119(2):023603, Jul 2017.

\bibitem{equiv}
Daniele Toniolo.
\newblock {On the equivalence of the Bott index and the Chern number on a
  torus, and the quantization of the Hall conductivity with a real space Kubo
  formula}.
\newblock {\em arXiv}, Aug 2017.

\bibitem{Hastings2009}
M.~B. Hastings and T.~A. Loring.
\newblock {Almost Commuting Matrices, Localized Wannier Functions, and the
  Quantum Hall Effect}.
\newblock {\em arXiv}, Oct 2009.

\bibitem{IEEE}
Ieee standard for binary floating-point arithmetic.
\newblock {\em ANSI/IEEE Std 754-1985}, pages 1--20, 1985.

\bibitem{Loring2010}
T.~A. Loring and M.~B. Hastings.
\newblock {Disordered Topological Insulators via $C^*$-Algebras}.
\newblock {\em arXiv}, May 2010.

\bibitem{anderson1958}
P.~W. Anderson.
\newblock {Absence of Diffusion in Certain Random Lattices}.
\newblock {\em Phys. Rev.}, 109(5):1492, Mar 1958.

\bibitem{anderson}
F.~Evers and A.~D. Mirlin.
\newblock {Anderson Transitions}.
\newblock {\em arXiv}, Jul 2007.

\bibitem{liu2020}
Gui-Geng Liu, Yihao Yang, Xin Ren, Haoran Xue, Xiao Lin, Yuan-Hang Hu,
  Hong-xiang Sun, Bo~Peng, Peiheng Zhou, Yidong Chong, and Baile Zhang.
\newblock {Topological Anderson Insulator in Disordered Photonic Crystals}.
\newblock {\em Phys. Rev. Lett.}, 125(13):133603, Sep 2020.

\end{thebibliography}
