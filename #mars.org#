#+TITLE: Daily report: topology VS disorder (MARS)
#+AUTHOR: Pierre Wulles
#+SETUPFILE: https://fniessen.github.io/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil
#+LATEX_HEAD: \usepackage{braket}
#+HTML_HEAD: <style> #content{max-width:1800px;}</style>
#+HTML_HEAD: <style> p{max-width:1600px;}</style>

*ARCHIVE MARS*
	 
* Paralléliser
** DONE Prise en main d'openMP
Voici le code de prise en main créé <2021-03-26 ven.>
#+BEGIN_SRC c++
#include <iostream>
#include <sys/time.h>
#include <vector>
#include "omp.h"

using namespace std;

int main(){
  const int M = 100000000, N = 100;
  //vector<double> D(M), E(M), F(M);
  double *D = new double[M];
  double *E = new double[M];
  double *F = new double[M];
  
  cout << omp_get_num_procs() << endl;
  
  double t0 = omp_get_wtime();
  double t0_cpu = clock();
# pragma omp parallel for 
  for(int i = 0; i < M; i++){
    D[i] = 10;
    E[i] = 100;
    F[i] = D[i]*E[i];
  }
  double t1 = omp_get_wtime();
  double t1_cpu = clock();
  cout << "Temps reel: " << t1-t0 << endl;
  cout << "Temps CPU: " << (t1_cpu-t0_cpu)/CLOCKS_PER_SEC << endl;

  delete D[];
  delete E[];
  delete F[];
  
  return 0;
}
#+END_SRC
Quelques remarques sur ce code:
- Utiliser *l'allocation dynamique* pour allouer de gros tableaux.
- Les tableaux /brutes/ sont bien plus performants que la classe
  ~vector~. Cela relance la question de programmer soi-même les
  routines pour produit matriciel et diagonalisation même si d'après
  /Numerical Recipies/ il s'agit de quelque chose de très compliqué.
- Il ne faut pas oublier de préciser le nombre de thread via la
  variable ~OMP_NUM_THREAD~.
- Pour une raison mystérieuse le code ne s'exécute pas avec tous les
  threads disponibles sur le cluster de calcul. Seulement 16.
** DONE Profiler
Avant de *paralléliser*, il faut *optimiser*, et avant d'optimiser, il
faut *profiler*.

On utilise l'outils ~valgrind~ avec l'option ~--tool=callgrind~ puis
on visualise à l'aide de ~kcachegrind~:

[[file:profile.png]]

On remarque que comme attendu c'est la diagonalisation qui prend
énormement de temps. Le calcul de l'indice de bott (qui inclut
également une diagonalisation) est également problématique,
probablement à cause des multiples produits matriciels. Cette fonction
est optimisable car les matrices $U_X$ sont recalculées à chaque
fois. Il faut améliorer ça.
** DONE Optimiser
*** DONE Découpage de la fonction de bott
<2021-03-30 mar.>
*** DONE Passer toutes les matrices par reférences
Avant:
#+BEGIN_SRC bash
➜  build git:(master) ✗ ../build/honeycomb.out 14 1 -12 0 0 0
m: 395
omega: -0.872248
IPR_m: 0.0133129
Temps reel: 12.5795
Temps CPU: 12.8841
#+END_SRC
Après: 
#+BEGIN_SRC bash
➜  build git:(master) ✗ ../build/honeycomb.out 14 1 -12 0 0 0
m: 395
omega: -0.872248
IPR_m: 0.0133129
Temps reel: 12.2738
Temps CPU: 12.5488
#+END_SRC
<2021-03-30 mar.>

** DONE Paralléliser tous les scripts SH au sein du programme C++
Plus question de faire des scripts sh pour lancer les instances mais
au contraire tous mettre dans le programme honeycomb puis lancer avec
la parallélisation les boucles. Pour cela il faudra profondément
modifier le main et réécrire pas mal de bout de fonction. 

<2021-03-30 mar.>

Très bonne performance et on peut désormais lancer le script sur le
cluster, facilement un facteur 100 voir 1000 entre les versions sh et
le script parallélisé sur le cluster.

*Attention* à bien compiler sur la machine qui exécute le script car
 avec l'option ~-march=native~ on spécifie pour un processeur et les
 processeurs du clusters ne sont pas tous les mêmes.

* Calcul de l'IPR et indice de bott désordoné
On cherche à calculer: 

$$\mathrm{IPR}_{m}=\sum_{j=1}^{N}\left\{\sum_{\mu=1}^{3}\left|\psi_{m}^{3(j-1)+\mu}\right|^{2}\right\}^{2}$$

En supposant que $\psi_m$ est normalisé:

$$\sum_{j=1}^{N} \sum_{\mu=1}^{3}\left|\psi_{m}^{3(j-1)+\mu}\right|^{2}=1$$

On trace une première carte en fonction de la fréquence et de l'erreur (entre 0 et 10%):

[[file:ipr.png]]

Cela n'est pas très concluant car la résolution est probablement bien
trop faible. Il faut améliorer le calcul avant tout. --> On obtient:

[[file:iprs_AB0_B12_N20.png]] [[file:botts_disorder_AB0_B12_N20.png]]

Si on trace la carte des valeurs propres liée à l'IPR on obtient: 

[[file:complex_ipr4.png]]

On peut tracer les cartes d'intensités pour les modes associés les plus élevés:

[[file:ipr_big2.png]][[file:ipr_big3.png]][[file:ipr_big4.png]][[file:ipr_big5.png]][[file:ipr_big6.png]][[file:ipr_big7.png]][[file:ipr_big8.png]][[file:ipr_big9.png]][[file:ipr_big10.png]]
* Perturber le réseau
** DONE Déplacer un atome 
[[file:intensity_map5.png]]
[[file:intensity_map5_pert.png]]
[[file:intensity_map6_pert.png]]
** DONE Déplacer tous les atomes 
[[file:intensity_map7.png]]
[[file:intensity_map7_pert_all.png]]

Pour étudier plus précisément le système on s'intéresse au calcul de
la carte d'indice de bott $\Delta_B/\Delta_{AB}$:

[[file:botts_map_disorder_deltaR_0.12.png]]

On voit apparaître une structure attendue. Le temps de calcul est très
long (10 minutes). On réduit en passant sur un réseau à 144 atomes. 

[[file:botts_map_disorder_deltaR_0.03_N_6.png]]

Le problème qui semble apparaître ici est que certains résultats qui
valent 0 devraient valoir 1 ! Une explication est peut-être que cette
carte est calculée à $\omega$ constant. On devrait la calculer pour
$\omega \in \text{GAP}$. Il faut donc trouver une façon de localiser
le GAP. --> Non il s'agit en fait d'un problème dans le script
d'exécution bash qui ne gère pas les nombres flottants !

Voici la version zoomé correcte:

[[file:botts_map2_disorder_ok.png]]


** DONE Tracer une carte frequence/perturbation
<2021-03-19 ven.>

[[file:map_freq_random_CB_annotation.png]]
* Indice de Bott pour un système ordonné
** DONE Calculer l'indice de bott
Partir de $X_{ij} = \delta_{ij} x_i$ et $Y_{ij} = \delta_{ij} y_i$
puis introduire: 

\begin{aligned}
V_X = \text{exp}\left(\frac{2\pi i}{L_X}X\right)
\end{aligned}
On reprend ensuite les états propres de:
\begin{aligned}
G\left|n\right > = \lambda \left|n\right >
\end{aligned}
Dans le projecteur 
\begin{aligned}
P = \sum_{\omega_n < \omega} \left |n\right >\left < n\right |
\end{aligned} Puis on projette pour X et Y:
\begin{aligned}
\widehat{V_X} = PV_XP
\end{aligned} On peut finalement calculer: 
\begin{aligned}
C_B(\omega) = \frac{1}{2\pi}\text{Im}(\text{Tr}(\text{log}(\widehat{V_Y}\widehat{V_X}\widehat{V_Y}^{\dagger}\widehat{V_X}^{\dagger})))
\end{aligned}
En utilisant l'identité: 
\begin{aligned}
\text{Tr(log)} = \text{log(det)}
\end{aligned}

$\delta$:

On suppose $M$ diagonalisable: $MP = PD$ et on suppose $P$
inversible. La formule est évidente pour une matrice diagonale, par
continuité on généralise à toutes matrices. Attention cependant si les
valeurs propres sont complexes, la formule est vraie seulement pour
une branche du logarithme.

Plusieurs tentatives pour le calcul de l'indice de bott, rien qui ne
ressemble à ce qui est attendu pour le moment <2021-03-12 ven.>

Finalement on a un problème de précision numérique probablement causé
par le grand nombre de multiplication dans le calcul du
déterminant. On emprunte un autre chemin, diagonalisation de la
matrice *puis* calcul du log et de la trace. *Succès !* <2021-03-17 mer.>

** DONE Tracer $C_B(\omega)$
[[file:bott_frise_gap.png]]

On trace aussi les cartes $\Delta_B/\Delta_{AB}$:

[[file:botts_map.png]]

On retrouve le résultat voulu !

On peut aller plus loin et tracer pour $\Delta_B/\Delta_{AB}$ négatifs:

[[file:botts_map2.png]]

* Étude approfondie du système hexagonal
** DONE Étudier le GAP pour \Delta_{B,AB} variable
Voici quelques graphes plus précis:
[[file:hist_deltaB0_N20.png][histdeltaB0N20]]

[[file:kde_deltaB0_N20.png][kdedeltaB0N20]]

[[file:hist_deltaB12_N20.png][histdeltaB12N20]]

[[file:kde_deltaB12_N20.png][kdedeltaB12N20]]

[[file:hist_deltaAB12_N20.png][histdeltaAB12N20]]

[[file:kde_deltaAB12_N20.png][kdedeltaAB12N20]]

*On ne voit pas de gap apparaître pour $\Delta_{AB}$*

Correction le <2021-03-10 mer.> Un gap apparait effectivement:
confusion dans l'ordre des sites !

[[file:kde_deltaAB12_N16_correct.png]]

On trace sur un même plot les DOS pour différentes valeurs de $\Delta_B$:

[[file:DOS_delta_variableN10.png]]

--> Peu utile car non aligné et trop dézoomé. à refaire.

On élabore une carte de la largeur du GAP en fonction de $\Delta_B$ et $\Delta_{AB}$.

[[file:gap_map2.png]]


** DONE Trier les vecteurs propres en fonctions des vp
On utilise la portion de code suivante:
#+BEGIN_SRC c++
    int ind = 0, ind_inf = 0;
  for(int i = 0; i < 4*NN; i++){
    ind_inf = ind + pos_minimum_vector(f_real.tail(4*NN-ind));
    double s;
    s = f_real(ind_inf);
    f_real(ind_inf) = f_real(ind);
    f_real(ind) = s;
    Q.col(ind).swap(Q.col(ind_inf));
    ind++;
  }

int pos_minimum_vector(VectorXd v){
  int n = v.rows(), ind = 0;
  double inf = v(0);
  for(int i = 0; i < n; i++){
    if (v(i) < inf) {
      inf = v(i);
      ind = i;
    }
  }
  return ind;
}

#+END_SRC
** DONE Tracer la carte d'intensité du réseau hexagonal
On utilise la portion de code suivante:
#+BEGIN_SRC c++
  for(int i = 0; i < 4*NN; i+=2){
	x1 = Q.col(ivector)(i);
	x2 = Q.col(ivector)(i+1);
	v = Vector2cd(x1,x2);
	Intensity = v.squaredNorm();
	total_int += Intensity;
	f_intensity_map <<  hexlattice[i/2].transpose()(0) << "," << hexlattice[i/2].transpose()(1) << "," << Intensity << "," << f_real(ivector) << endl;
  }
#+END_SRC
Après traitement des données dans python on obtient:

[[file:heatmap.png][wrong]]

Ce qui ne correspond pas du tout à ce qu'on attendait ! Bien que l'on ait en effet:
$$\sum_n I_n = 1$$

Plusieurs erreur ont causé ce problème, déjà ce qui était tracé
n'était pas vraiment exact, car il s'agissait de tracer pour un
vecteur propres donnés les 2*NN intensités. Bref le code ne traçait
pas du tout ce qu'il fallait.

Après plusieurs avoir bien annalysé le code mathematica, j'ai décidé
d'utiliser la librairie python seaborn pour visualiser avec un beau
rendu les cartes d'intensités du réseau hexagonal. Voici quelques
exemples de cartes obtenues: (<2021-03-15 lun.>)

[[file:intensity_map1.png]]
[[file:intensity_map2.png]]
[[file:intensity_map3.png]]
[[file:intensity_map4.png]]

** DONE Localiser le GAP en traçant 
<2021-03-23 mar.> Création du programme gap_finder. Utilise la
librairie boost pour faire l'histogramme. Trouve le GAP en le
cherchant dans une zone bien précise.

*à améliorer* encore sensible à des gaps qui n'en sont pas quand il y
 a un pic au milieu du pseudo gap par exemple: il faut vérifier que la
 DOS ne repasse jamais au dessus d'une certaine valeur.

** DONE Tracer le GAP($\Delta_B$)

On obtient le graphe suivant pour $\Delta_{AB} = 5$:

[[file:coupe_gap_deltaAB5.png]]
** DONE Exporter les résultats en GIF
* DOS d'un réseau hexagonal 
** DONE Réobtenir le diagramme de bande à partir d'un code C++
- Le git est mis en place [[https://gitlab.com/starcluster/honeycomb-lattice-of-atoms][ici]]. <2021-03-01 lun.>
- Ajout d'un fichier d'autoconfiguraton cmake. <2021-03-02 mar.>
- Choix du paradigme objet. <2021-03-02 mar.>
- Réseau hexagonal généré. <2021-03-02 mar.>

*** DONE Création du réseau hexagonal
Implémentation des vecteurs $a1$, $a2$, $a3$.

Utiliser directement une structure objet pour stocker le type d'atome
$A$ ou $B$ ? Oui.

Avoir un object de type *site* avec un Vector2d et un type
d'atome. Puis un objet *hexgrid* avec un constructeur qui génère la
grille en fonction d'un paramètre de taille $N$.

Finalement on obtient la figure suivante:

 [[file:hex.png]] 

Tracée avec gnuplot:

#+BEGIN_SRC bash
plot "A.dat" pt 7 ps 1 lc rgb "blue", "B.dat" pt 7 ps 1 lc rgb "red"
#+END_SRC


Le réseau est généré par itération des vecteurs $a1$, $a2$ et $a3$ sur
le vecteur nul. On remarque que à $N$ sites donné, le réseau est
rectangulaire. Les couleurs sont obtenues en considérant la parité de
$i+j$.

Code pour générer le réseau:
#+BEGIN_SRC C++
Vector2d vect_i(0,0);
  for(int i = 0; i < N; i+=2){
    Grid[i*N] = vect_i;
    vect_i += vertical;
    Grid[(i+1)*N] = vect_i;
    vect_i += a1;
  }
  for(int i = 0; i < N; i++){
    vect_i = Grid[i*N];
      if (i%2==0){
	for(int j = 0; j < N-2; j+=2){
	vect_i += -a2;
	Grid[i*N + j+1] = vect_i;
	vect_i += a3;
	Grid[i*N + j+2] = vect_i;
	}
	Grid[i*N + N-1] = vect_i - a2;
      }
      else{
	for(int j = 0; j < N-2; j+=2){
	  vect_i += a3;
	  Grid[i*N + j+1] = vect_i;
	  vect_i += -a2;
	  Grid[i*N + j+2] = vect_i;
	}
	Grid[i*N + N-1] = vect_i + a3;
      }
  }
#+END_SRC


Coder les fonctions *nn* et *nn2*.
*** DONE Diagonaliser la matrice $\widehat{G}$
Le hamiltonien du système est donné par: 

\begin{aligned}
H &=\hbar \sum_{n=1}^{N} \sum_{\alpha=\sigma_{+}, \sigma_{-}}\left[\omega_{0} \pm \delta \omega_{0}+\operatorname{sgn}\left(\alpha_{i}\right) \mu_{B} B-i \frac{\Gamma_{0}}{2}\right] \\
& \times\left|\alpha_{n}\right\rangle\left\langle\alpha_{n}\right| \\
&+\frac{3 \pi \hbar \Gamma_{0}}{k_{0}} \sum_{n \neq m}^{N} \sum_{\alpha, \beta=\sigma_{+}, \sigma_{-}} \mathcal{G}_{\alpha \beta}\left(\mathbf{r}_{n}-\mathbf{r}_{m}\right)\left|\alpha_{n}\right\rangle\left\langle\beta_{m}\right|
\end{aligned}

où le signe devant $\delta \omega_0$ dépend du sous-réseau $A$ ou
$B$. On introduit la matrice $3N\times 3N$ qui est fait de bloc
$3\times 3$:

$$
\hat{G}_{m n}=\left(i \pm 2 \Delta_{A B}-2 \alpha \Delta_{\mathrm{B}}\right) \delta_{m n} \mathbb{1}+\hat{d}_{e g} \hat{A}_{m n} \hat{d}_{e g}^{\dagger}
$$

où: 
- $\Delta_{AB} = \delta \omega_0/\Gamma_0$ est la fréquence entre $A$ et $B$
- $\Delta_{B} = \mu_B B/\Gamma_0$ est le shift Zeeman

Et:

\begin{aligned}
\hat{A}_{m n} &=\left(1-\delta_{m n}\right) \frac{e^{i k_{0} r}}{k_{0} r}\left[P\left(i k_{0} r_{m n}\right) \mathbb{1}\right.\\
&\left.+Q\left(i k_{0} r_{m n}\right) \frac{\mathbf{r}_{m n} \otimes \mathbf{r}_{m n}}{r_{m n}^{2}}\right]
\end{aligned}

Avec:
- $k_{0}=\omega_{0} / c$
- $\mathbf{r}_{m n}=\mathbf{r}_{m}-\mathbf{r}_{n}$
- $P(x)=1-1 / x+1 / x^{2}$
- $Q(x)=-1+3 / x-3 / x^{2}$

Et:

$$
\hat{d}_{e g}=\left[\begin{array}{ccc}
\frac{1}{\sqrt{2}} & \frac{i}{\sqrt{2}} & 0 \\
-\frac{1}{\sqrt{2}} & \frac{i}{\sqrt{2}} & 0 \\
0 & 0 & 1
\end{array}\right]
$$

La matrice est implémentée est le code tourne pour calculer des
valeurs propres, reste à savoir si elles sont correctes !

Problèmes rencontrés:
- problème de typage dans tous les sens
- dépassement de mémoire
- confusions sur le réseau hexagonal

Temps d'exécution pas si rapide, pour 100 atomes (G est une matrice
300x300):

#+BEGIN_SRC bash
./honeycomb.out  14,90s user 0,01s system 99% cpu 14,958 total
#+END_SRC

*** DONE Tracer les parties réelles des valeurs propres *(incoherent)*
<2021-03-03 mer.>

Maintenant il faut vérifier que tout cela est correct commençons par
tracer dans le plan complexe les valeurs propres:

[[file:eigenvalues.png]]

On otient pas de moyen de conclure, traçons la densité de valeur propre
d'après l'équation:

$$\omega  = \omega_0 - \frac{\Gamma_0}{2}\text{Re}(\Lambda)$$

On obtient pour 20x20 atomes:

[[file:eig_hist_20x20.png]]

On observe pas de gap ce qui est inquiétant. 

Pistes à explorer: 
- regarder ce qui se passe pour des pas différents $a$. Pour un pas
  très grand devant la longueur d'onde toutes les valeurs propres
  devraients se concentrer en (0,1).
- revérifier minutieusement le code.
- mettre en place un pipeline pour tracer facilement le résultat.

*** DONE Complexité du code
Opération la plus longue: trouver les valeurs propres de la matrice
$G$, décomposition LU globalement en $n^3$ (voir [[https://math.stackexchange.com/questions/2820546/time-complexity-reducing-square-matrix-to-diagonal-matrix-using-gaussian-row][ici]]). Correspond à
l'observation. [[file:diagonalize_complexity.jpeg]]

On peut tracer le graphe pour quelques valeur de $N$ ($N\times N$
étant alors le nombre d'atomes).

[[file:complexity_exp.png]]

On effectue le fit sur la fonction $x\mapsto ax^3 + bx^2 + cx$:

#+BEGIN_SRC bash
After 7 iterations the fit converged.
final sum of squares of residuals : 601.931
rel. change during last iteration : -3.68883e-12

degrees of freedom    (FIT_NDF)                        : 1
rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 24.5343
variance of residuals (reduced chisquare) = WSSR/ndf   : 601.931

Final set of parameters            Asymptotic Standard Error
=======================            ==========================
a               = 0.35129          +/- 0.05365      (15.27%)
b               = -6.21042         +/- 1.586        (25.53%)
c               = 26.6032          +/- 10.89        (40.94%)

correlation matrix of the fit parameters:
                a      b      c      
a               1.000 
b              -0.992  1.000 
c               0.948 -0.980  1.000
#+END_SRC
Cela nous permettra à l'avenir d'estimer grossièrement le temps d'attente.

Code gnuplot utilisé pour faire ce fit (toujours utile):

#+BEGIN_SRC bash
set xlabel "Size of the grid (side = n)"
set ylabel "Execution time [s]"
f(x) = a*x**3 + b*x**2 + c*x # Complexite en O(n^3) sans offset
fit f(x) 'time_honeycomb.dat' u 1:2 via a,b,c
plot f(x), 'time_honeycomb.dat' u 1:2 pt 7 ps 2
#+END_SRC
*** DONE Mise en place du pipeline
<2021-03-03 mer.>
Il faut passer les paramètre en ligne de commande, on utilise la
syntaxe suivante:
#+BEGIN_SRC c++
  if (argc > 1){
    istringstream iss(argv[1]);
    if (iss >> N){}
    else{cout << "Invalid parameter! ABORT"<<endl; abort();}
  }
#+END_SRC

 On respecte le schéma suivant: [[file:pipeline.png]]

Le pipeline calcule également un temps estimé pour honeycomb.out et
donne le temps effectif: (dépend de ~bc~)

#+BEGIN_SRC bash 
echo Launching honeycomb.out with N = $1 and a =$2
et=$( echo 0.35*$1*$1*$1 - 6*$1*$1 + 26*$1 | bc)
echo Estimated time: $et
start=`date +%s.%N`
../build/honeycomb.out $1 $2 > tmp.dat # computing eigenvalues
sed 's/[(,)]/ /g' tmp.dat > tmp2.dat # parsing 
awk '{print $1/2}' tmp2.dat > tmp3.dat # get real part and divide by 2
gnuplot hist.gnuplot -p # plotting 
rm tmp.dat tmp2.dat tmp3.dat # cleaning
end=`date +%s.%N`
runtime=$( echo "$end - $start" | bc -l )
echo Computing time: $runtime
echo end-start | bc
#+END_SRC
*** DONE Retracer DOS à partir du code mathematica
<2021-03-05 ven.>
Après quelques erreurs d'allocation mémoire c'est un succès: 

[[file:hist_new_honeycomb.png]]
[[file:eig_new_honeycomb.png]]

On réobtient bien les DOS de mathematica.
Comparaison plus précise pour $N=800$ atomes et $\Delta_B = 12$:

[[file:hist_new_honeycomb_800.png]]
[[file:hist_sergey.png]]

Quand même assez long: 30 minutes pour 800 atomes !
*** DONE Optimiser le programme.
<2021-03-06 sam.>
On commence par jouer sur les options de gcc, pour une entrée $N=10$
on obtient les résultats suivants:
| Options de compilation | Temps d'exécution en s | Taille du binaire en MégaOctets |
|------------------------+------------------------+---------------------------------|
| Rien                   |                     33 |                              15 |
| -O1                    |                    1.8 |                              16 |
| -O2                    |                    1.2 |                              17 |
| -O3                   |                    1.9 |                              17 |
On choisit de conserver -O2 pour la suite (voir [[https://gcc.gnu.org/onlinedocs/gcc-4.1.1/gcc/Optimize-Options.html][ici]] pour une
description exacte de cette option).

Utilisation d'openMP pas d'améliorations notables, à peine quelques
dixièmes de seconde, l'algorithme d'eigen3 n'est peut-être pas concçu
pour fonctionner paralléliser ... à creuser.
** DONE Diagonaliser une matrice avec eigen:
<2021-03-01 lun.>
#+BEGIN_SRC C++
   Matrix2f A;
   A << 1, 2.5, 2.5, 3;
   cout << "Here is the matrix A:\n" << A << endl;
   SelfAdjointEigenSolver<Matrix2f> eigensolver(A);
   if (eigensolver.info() != Success) abort();
   cout << "The eigenvalues of A are:\n" << eigensolver.eigenvalues() << endl;
   cout << "Here's a matrix whose columns are eigenvectors of A \n"
        << "corresponding to these eigenvalues:\n"
        << eigensolver.eigenvectors() << endl;
#+END_SRC 
** DONE Installer Mathematica player 
<2021-03-01 lun.>
* Préliminaires
** DONE Établir une base bibliographique
Article de référence: [[https://arxiv.org/abs/1703.04849][Topological Quantum Optics in Two-Dimensional Atomic Arrays]]

On a aussi:
- [[https://arxiv.org/abs/0712.1776][Reflection-Free One-Way Edge Modes in a Gyromagnetic Photonic Crystal]]

** TODO Benchmark diagonalisation ?
Faire un benchmark python/julia/c++ eigen/c++ perso.
** DONE Installation de eigen3 et getting started
<2020-12-22 mar.>

Eigen3 possède une API élégante, est rapide et récente, bien
maintenue, écrite en c++, je la choisis en remplacement de LAPACK pour
les opérations d'algèbre linéaire.

Doc [[http://eigen.tuxfamily.org/index.php?title=Main_Page][ici.]]

Installation: ~pacman -S eigen~

Exemple de base:
#+BEGIN_SRC C++
#include <iostream>
#include <Eigen/Dense>
 
using Eigen::MatrixXd;
 
int main()
{
  MatrixXd m(2,2);
  m(0,0) = 3;
  m(1,0) = 2.5;
  m(0,1) = -1;
  m(1,1) = m(1,0) + m(0,1);
  std::cout << m << std::endl;
}
#+END_SRC 
à compiler avec : ~g++ -I /usr/include/eigen3/ getting_started.cpp -o a~
** DONE Installation de LAPACK et getting started
<2020-12-22 mar.>

Installé mais après quelques tests compiler du fortran depuis le c++
est vraiment pénible et l'API n'est pas terrible . D'après cette [[https://stackoverflow.com/questions/1380371/what-are-the-most-widely-used-c-vector-matrix-math-linear-algebra-libraries-a][page]]
lapack est vieux comme un /dinosaure/ et n'est même pas si performant
qu'on pourrait le penser: *ABANDON de cette solution*
** DONE Mise en place de cette page
<2020-12-22 mar.>

Publier sur gitlab.
** DONE Meep premier programme c++
<2020-12-22 mar.>

Premier programme c++ à l'aide de meep en suivant la doc:
https://meep.readthedocs.io/en/latest/C%2B%2B_Tutorial/

On utilise:
#+BEGIN_SRC c++
#include <meep.hpp>
#include <complex>
using namespace meep;
using namespace std;
#+END_SRC

** DONE Installation de Meep 
<2020-12-21 lun.>

Installé via le package AUR: https://aur.archlinux.org/packages/meep

Dépend de 
- julia (langage avec syntaxe proche de python, performance proche du
  c++)
- MPI (bibliothèque de programmation parallèle)
- h5utils (pour visualiser les fichiers hdf5)
- harminv (résoudre le programme de décomposition d'un signal
  quelconque en somme de sinusoïdes décroissantes),
- libctl (controle de fichier pour les simulation scientifiques)
- swig (pour générer l'interface de contrôle avec python ou Scheme)
