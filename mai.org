#+TITLE: Daily report: topology VS disorder (MAI)
#+AUTHOR: Pierre Wulles
#+SETUPFILE: https://fniessen.github.io/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil
#+LATEX_HEAD: \usepackage{braket}
#+HTML_HEAD: <style> #content{max-width:1800px;}</style>
#+HTML_HEAD: <style> p{max-width:1600px;}</style>

*ARCHIVE MAI*
	 
* Changer la grille carré -> parrallélogramme
- refaire pour les sites B la tache
<2021-05-25 mar.> Un GAP semble quand même s'ouvrir ... --> *Non il ne*
*s'agit pas vraiment d'un GAP car on ne voit rien sur le DOS !*

[[file:sitesB_Ngrowing.png]]

- pour les bandes n'afficher les points qu'aux points ou l4IPR fait sens

[[file:bandes_ipr2.png]]



- calculer les bandes IPR pour le parallélogramme

[[file:bandes_ipr_parall.png]]

- reproduire la figure deltaB/deltaAB sans désordre! pour w = 7
  <2021-05-20 jeu.>
[[file:parall_bott_map.png]]

un résultat très surprenant ! on a un décalage par rapport à la figure
obtenue.
- confirmer la disparition de la tache

La tâche semble belle et bien disparaître pour $N$ croissant.
  [[file:B5AB5N10.png]]
  [[file:B5AB5N14.png]]
  [[file:B5AB5N16.png]]
  [[file:B5AB5N18.png]]


  
- zoomer sur une bande IPR (comparaison gap trivial/gap topologique)

  [[file:bandes_ipr.png]]

On observe des plateaux oscillants: pas logique les plateaux devraient
être constants, on recalcule l'IPR pour la même fréquence à chaque
fois ! --> Solution le <2021-05-25 mar.> le problème venait du
parallélisme: en effet les IPR ne sont pas tous calculés par les mêmes
threads et un arrondi doit être fait pour diagonaliser la matrice. En
enlevant le // le problème disparait.


- tester les bords arm chair dans le cas deltaAB =! 0

<2021-05-21 ven.>

* Restructuration du code
<2021-05-12 mer.> Suite au changement de type de grille (de carré vers
hexagonal) j'ai pu constater combien cette évolution était difficile,
les algorithmes que j'avais écrit n'étaient pas facilement adaptable à
une grille différente (boucle faites pour tourner sur des grilles). Il
faut dont réécrire et restructurer (en évitant au plus possible
l'utilisation des types matrix spéciaux car ils font bien bugger
valgrind) le code en amont de la partie qui ne conserve que le calcul
des valeurs propres. Le code ~honeycomb~ doit donc être pensé en deux étapes:
- Créer er diagonaliser l'hamiltonien
- Exploiter l'hamiltonien

Afin de faciliter l'implémentation je prends la décision de conserver
le programme tel quel est de lui ajouter la possibilité de charger des
coordonnées d'atomes générée par un programme tier plutôt que de les
générer lui même.

Pour se faire le paramètre utilisé *ne doit plus* être $N$ ou $NN$
mais $N_{atoms}$.

<2021-05-19 mer.>

Les modifications décrites précedemment ont été implantées, ce fut
laborieux, pour la suite je vais découper le monolithique programme
honeycomb en plusieurs petits programmes qui appelle les fonctiosn
écrites car cela devient ingérable.

* Changer la grille carré -> hexagonale
<2021-05-05 mer.> On prend la décision de passer d'une grille
rectangulaire à une grille hexagonale. En effet il semble que les
effets causés par les bords; arm-chair type ou zig-zag semblent
beaucoup influencer le comportement du système. On obtient les
résultats suivants après un bon moment de galère, car générer une
carte hexagale n'est pas si simple. La méthode finale retenue est du
type de découpe dans la grille rectangulaire.

[[file:hex_1364B12AB0.png]]
[[file:hex_1364B0AB12.png]]
file:hex_1364B0AB0.png

D'après cette grille:

[[file:hex_hex2.png]]
